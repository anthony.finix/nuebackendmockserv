const { admins } = require("../../db.js");
module.exports = (req, res) => {
  const { role } = req.body;
  const { id } = req.params;
  let admin = admins.find((admin) => admin.id == id);
  if (admin) {
    if (role === 1) {
      admin.role = "admin";
    } else if (role === 2) {
      admin.role = "owner";
    } else {
      admin.role = "read_only_admin";
    }
    return res.json({ data: admin });
  } else {
    res.json("no admin found");
  }
};
