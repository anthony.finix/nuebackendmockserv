const { admins } = require("../../db.js");

module.exports = (req, res) => {
  const { id, role, status } = req.query;
  let response = { data: admins };
  if (role)
    response.data = response.data.filter((admin) => admin.role === role);

  if (status)
    response.data = response.data.filter((admin) => admin.status === status);

  return res.json(response);
};
