const { admins } = require("../../db.js");
module.exports = (req, res) => {
  const { query } = req.query;
  const regex = new RegExp(`${query}`, "g");
  let data = admins.filter((admin) =>
    regex.test(`${admin.first_name} ${admin.last_name}`)
  );
  return res.json({ data: data });
};
