const express = require("express");
const cors = require("cors");
// admin
const getAdmin = require("./admin/services/getAdmin");
const searchAdmin = require("./admin/services/searchAdmin");
const updateAdmin = require("./admin/services/updateAdmin");
// users
const getUsers = require("./user/services/getUsers");
const searchUsers = require("./user/services/searchUsers");
// const updateUsers = require("./user/services/updateUsers");

const app = express();
app.use(cors());
app.use(express.json());
app.use((req, res, next) => {
  console.log(req.url);
  next();
});

// user
app.get("/users", getUsers);
// app.put("/users", updateUsers);
app.get("/users/search", searchUsers);

// admin
app.get("/admins/list", getAdmin);
app.patch("/admins/edit/:id", updateAdmin);
app.get("/admins/search", searchAdmin);

app.listen(8080, () => console.log("Listening to 8080"));
