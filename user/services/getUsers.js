const { users } = require("../../db.js");

module.exports = (req, res) => {
  const { id } = req.query;
  if (!!id || id === "0") {
    let user = users.find((user) => user.ID === parseInt(id));
    if (user) return res.json({ data: user });
    if (!user) return res.send("no data found");
  }
  return res.json({ data: users });
};
