const { users } = require("../../db.js");
module.exports = (req, res) => {
  const { query } = req.query;
  const regex = new RegExp(`${query}`, "g");
  let data = users.filter((user) =>
    regex.test(`${user.first_name} ${user.last_name}`)
  );
  return res.json({ data: data });
};
