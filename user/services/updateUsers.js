const { admins } = require("../../db.js");
module.exports = (req, res) => {
  const { status, id } = req.body;
  let index;

  admins.find((admin, i) => {
    if (admin.ID === parseInt(id)) index = i;
  });

  if (index) {
    admins[index].Status = status;
    res.json(admins[index]);
  } else {
    res.json("no admin found");
  }
};
